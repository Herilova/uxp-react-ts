import './styles.css';
import { entrypoints } from 'uxp';
import { CommandMap, Commands } from './commands/commands';
import { CommandController } from './controllers/CommandController';
import { PanelController } from './controllers/PanelController';
import { MenuItems } from './interfaces/UxpMenuItems.interface';
import { RT } from './interfaces/UxpRT.interface';
import { Panels } from './panels/panels';

const commands = Commands.reduce<CommandMap>((acc, { component, ...command }) => {
  acc[command.id] = new CommandController(component, command);
  return acc;
}, {} as CommandMap);

const panels = Panels.reduce((acc, { component, menuItems, ...panel }) => {
  const newMenuItems = menuItems.reduce<MenuItems[]>((ac, menuItem) => {
    const oninvoke = (): RT => menuItem.oninvoke(commands);
    ac.push({ ...menuItem, oninvoke });
    return ac;
  }, []);
  acc[panel.id] = new PanelController(component, { ...panel, menuItems: newMenuItems });
  //
  return acc;
}, {});

entrypoints.setup({
  plugin: {
    create(plugin) {
      /* optional */ console.log('created', plugin);
    },
    destroy() {
      /* optional */ console.log('destroyed');
    },
  },
  panels,
});
