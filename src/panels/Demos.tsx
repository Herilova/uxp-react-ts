import * as CryptoJS from 'crypto-js';
import React, { createRef, useState } from 'react';
import Button from '../components/buttons/Button';
import Body from '../components/typography/Body';
import Heading from '../components/typography/Heading';

const photoshop = require('photoshop');
const uxp = require('uxp');

const { app } = photoshop;
const fs = uxp.storage.localFileSystem;
const format = uxp.storage.formats;
const { executeAsModal } = photoshop.core;

export const Demos: React.FC = () => {
  const ref = createRef<HTMLImageElement>();
  const [hash, setHash] = useState('');
  const generateCurrentImage = async () => {
    try {
      const tempFolder = await fs.getTemporaryFolder();
      // tempFile
      const tempFile = await tempFolder.createFile('test.png', { overwrite: true });
      const { activeDocument } = app;
      // save tempFile in tempFolder
      await activeDocument.saveAs.png(tempFile, {
        compression: 9,
      });
      // metadata contains name, dateModified, createdDate, size
      // read tempFile as binary
      const fileData = await tempFile.read({ format: format.binary });
      const uint8Array = new Uint8Array(fileData);

      // create base64 of image
      const base64 = Buffer.from(uint8Array).toString('base64');
      const base64Url = `data:image/png;base64,${base64}`;

      // create blob of image
      const blob = new Blob([uint8Array], { type: 'image/png' });
      const blobUrl = URL.createObjectURL(blob);
      return {
        blob,
        blobUrl,
        base64,
        base64Url,
      };
    } catch (error) {
      return undefined;
    }
  };

  const handleExecuteAsModal = async (funtionValue: () => Promise<void>, modalName: string) => {
    await executeAsModal(funtionValue, { commandName: modalName });
  };

  const handleGenerateImage = () => {
    handleExecuteAsModal(async () => {
      try {
        const { base64, blobUrl } = await generateCurrentImage();
        if (ref.current) {
          ref.current.src = blobUrl;
        }
        // Conversion de l'image base64 en chaîne binaire
        const binaryImage = atob(base64);
        // Calcul du hachage SHA-256
        const hash256 = CryptoJS.SHA256(binaryImage);

        // Affichage du hachage en hexadécimal
        const hashValue = hash256.toString(CryptoJS.enc.Hex);
        console.log('hashValue ', hashValue);
        setHash(hashValue);
      } catch (error) {
        console.log(error);
      }
    }, 'Generation hashImage');
  };
  return (
    <div>
      <Heading>Plugin</Heading>
      <Button variant="cta" onClick={handleGenerateImage}>
        Generation Hash
      </Button>
      <Body>{`Hash : ${hash}`}</Body>
      <img alt="" ref={ref} width="100%" height="auto" />
    </div>
  );
};
