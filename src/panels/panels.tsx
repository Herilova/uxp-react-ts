import React from 'react';
import { CommandMap } from '../commands/commands';
import { ControllerComponent, ControllerProps } from '../interfaces/Entrypoints.interface';
import { MenuItems } from '../interfaces/UxpMenuItems.interface';
import { RT } from '../interfaces/UxpRT.interface';
import { Demos } from './Demos';

type InvokeFn = (controllers: CommandMap) => RT;

export type IPanels = Omit<ControllerProps, 'menuItems'> & {
  menuItems?: (Omit<MenuItems, 'oninvoke'> & { oninvoke: InvokeFn })[];
} & {
  component: ControllerComponent;
};

export const PANNEL_IDS = Object.freeze({
  DEMOS: 'demos',
});

export const Panels: Readonly<IPanels[]> = [
  {
    id: PANNEL_IDS.DEMOS,
    component: () => <Demos />,
    menuItems: [
      {
        id: 'reload1',
        label: 'Reload Plugin',
        enabled: true,
        checked: false,
        oninvoke: () => location.reload(),
      },
      {
        id: 'dialog1',
        label: 'About this Plugin',
        enabled: true,
        checked: false,
        oninvoke: controller => controller.showAbout.run(),
      },
    ],
  },
];
