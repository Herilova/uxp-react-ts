import React from 'react';
import { About } from '../components/About';
import { CommandController } from '../controllers/CommandController';
import { ControllerComponent, ControllerProps } from '../interfaces/Entrypoints.interface';

export const COMMAND_IDS = Object.freeze({
  SHOW_ABOUT: 'showAbout',
});
type CommandId = (typeof COMMAND_IDS)[keyof typeof COMMAND_IDS];
export type CommandMap = Record<CommandId, CommandController>;

interface ICommands extends ControllerProps {
  component: ControllerComponent;
}

export const Commands: Readonly<ICommands[]> = [
  {
    id: COMMAND_IDS.SHOW_ABOUT,
    title: 'React Starter Plugin Demo',
    size: { width: 480, height: 480 },
    component: ({ dialog }) => <About dialog={dialog} />,
  },
];
